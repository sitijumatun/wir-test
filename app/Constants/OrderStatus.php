<?php


namespace App\Constants;


class OrderStatus extends AbstractAppConstant
{
    public const PENDING = 1;
    public const COMPLETED = 2;
    public const CANCELLED = 3;
}
