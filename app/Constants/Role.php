<?php


namespace App\Constants;


class Role extends AbstractAppConstant
{
    public const MERCHANT = 1;
    public const CUSTOMER = 2;
}
