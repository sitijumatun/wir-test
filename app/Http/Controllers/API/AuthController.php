<?php

namespace App\Http\Controllers\API;

use App\Exceptions\TestException;
use App\Services\UserProcess;
use App\Transformers\UserTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $userService;
    public function __construct()
    {
        $this->userService = new UserProcess;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     * @throws TestException
     */
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'role' => 'required',
        ]);

        $userStore = $this->userService->storeData($request->all());

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Register Successfully',
            'payload' => fractal($userStore, new UserTransformer())->toArray(),
        ], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        $user = $request->user();

        return response()->json([
            'status'   => 'success',
            'message'  => 'login success',
            'code'     => 200,
            'payload' => fractal($user, new UserTransformer())->toArray(),
        ], 200);
    }


}
