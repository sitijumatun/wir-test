<?php

namespace App\Http\Controllers\API;

use App\Exceptions\TestException;
use App\Services\OrderProcess;
use App\Transformers\CustomerTransformer;
use App\Transformers\OrderTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class OrderController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws TestException
     * @throws ValidationException
     */
    public function buy(Request $request)
    {
        $this->validate($request,[
            'sum_of_item' => 'required',
            'product_id' => 'required',
            'payment_gateway' => 'required'
        ]);

        $buyProduct = new OrderProcess($request->user());
        $order = $buyProduct->buy($request->all());

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Order Success!',
            'payload' => fractal($order, new OrderTransformer())->toArray(),
        ], 200);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function listCustomer(Request $request){
        $listCustomer = new OrderProcess($request->user());
        $customers = $listCustomer->listCustomer();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Customer List Retrieved!',
            'payload' => fractal($customers, new CustomerTransformer())->toArray(),
        ], 200);

    }
}
