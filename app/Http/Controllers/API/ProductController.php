<?php

namespace App\Http\Controllers\API;

use App\Exceptions\TestException;
use App\Product;
use App\Services\ProductProcess;
use App\Transformers\ProductTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class ProductController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request){
        $productList = new ProductProcess($request->user());
        $products = $productList->index();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Product List Retrieved!',
            'payload' => fractal($products, new ProductTransformer())->toArray(),
        ], 200);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws TestException
     * @throws ValidationException
     */
    public function store(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'price' => 'required'
        ]);

        $productSave = new ProductProcess($request->user());
        $product = $productSave->store($request->all());

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Product Berhasil Disimpan',
            'payload' => fractal($product, new ProductTransformer())->toArray(),
        ], 200);
    }

    /**
     * @param Product $product
     * @param Request $request
     * @return JsonResponse
     * @throws TestException
     * @throws ValidationException
     */
    public function update(Product $product, Request $request){
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'price' => 'required'
        ]);

        $productUpdate = new ProductProcess($request->user());
        $product = $productUpdate->update($product, $request->all());

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Product Berhasil Diupdate',
            'payload' => fractal($product, new ProductTransformer())->toArray(),
        ], 200);
    }

    /**
     * @param Product $product
     * @param Request $request
     * @return JsonResponse
     * @throws TestException
     */
    public function delete(Product $product, Request $request){
        $productDelete = new ProductProcess($request->user());
        $product = $productDelete->delete($product);

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Product Berhasil DiDelete',
            'payload' => [],
        ], 200);
    }
}
