<?php

namespace App\Http\Controllers\Web;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class AlgoritmaController extends Controller
{
    /**
     * @param Request $request
     * @return Factory|View
     */
    public function programSatuA(Request $request){
        $numberLoop = $request->has('number')
            ? $request->get('number')
            : 0;

        return view('algoritma.soal-satu-a', compact('numberLoop'));
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function programSatuB(Request $request)
    {
        $numberLoop = $request->has('number')
            ? $request->get('number')
            : 0;

        return view('algoritma.soal-satu-b', compact('numberLoop'));
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function programDua(Request $request)
    {
        $countInput = $request->has('number')
            ? strlen($request->get('number'))
            : 0;

        $inputData = $request->has('number')
            ? $request->get('number')
            : 0;

        return view('algoritma.soal-dua', compact('countInput', 'inputData'));
    }
}
