<?php

namespace App\Http\Middleware;

use App\Exceptions\AuthException;
use App\Exceptions\TestException;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class Role
{
    /**
     * Handle an incoming request.
     * @param Request $request
     * @param \Closure $next
     * @param array $roles
     * @return mixed
     * @throws AuthException
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if (!in_array($request->user()->role, $roles)) {
            throw new AuthException('Unauthorized: insufficient role');
        }

        return $next($request);
    }
}
