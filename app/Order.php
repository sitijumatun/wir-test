<?php

namespace App;

use App\Scopes\CustomerScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Order extends Model
{
    protected $fillable = [
        'sum_of_item',
        'current_price_per_item',
        'status',
        'total_amount'
    ];

    protected static function boot(): void
    {
        parent::boot();
        static::addGlobalScope(new CustomerScope);
    }

    /**
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function product(){
        return $this->belongsTo(Product::class);
    }

    /**
     * @return HasOne
     */
    public function payment(){
        return $this->hasOne(Payment::class);
    }

    /**
     * @return HasOne
     */
    public function reward(){
        return $this->hasOne(Reward::class);
    }
}
