<?php


namespace App\Scopes;


use App\Constants\Role;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class UserScope implements Scope
{

    /**
     * @inheritDoc
     */
    public function apply(Builder $builder, Model $model)
    {
        // TODO: Implement apply() method.
        $scopedRoles = [
            Role::MERCHANT,
        ];

        if (auth()->user() && in_array(auth()->user()->role, $scopedRoles, true)) {
            $builder->where('user_id', auth()->user()->id);
        }
    }
}
