<?php


namespace App\Services;


use App\User;

class AbstractServiceConstant
{
    protected $user;
    public function __construct(User $user){
        $this->user = $user;
    }
}
