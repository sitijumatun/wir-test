<?php


namespace App\Services;


use App\Constants\OrderStatus;
use App\Constants\Role;
use App\Exceptions\TestException;
use App\Order;
use App\Payment;
use App\Product;
use App\Reward;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class OrderProcess extends AbstractServiceConstant
{
    /**
     * @param $data
     * @return Order
     * @throws TestException
     */
    public function buy($data){
        try {
            DB::beginTransaction();

            $product = $this->checkProductAvailabilty($data['product_id']);
            $order = $this->storeOrder($product, $data);
            $this->createPayment($order, $data);
            $this->checkReward($order);

            DB::commit();

            return $order;
        }catch (\Exception $e){
            DB::rollBack();
            throw new TestException($e->getMessage());
        }

    }


    /**
     * @param $product_id
     * @return mixed
     * @throws TestException
     */
    public function checkProductAvailabilty($product_id){
        $product = Product::find($product_id);
        if($product == null){
            throw new TestException('Product Tidak Tersedia');
        }

        return $product;
    }

    /**
     * @param Product $product
     * @param $data
     * @return Order
     * @throws TestException
     */
    protected function storeOrder(Product $product, $data)
    {
        $totalAmount = (int) $data['sum_of_item'] * $product->price;
        $order = new Order();
        $order->fill($data);
        $order->total_amount = $totalAmount;
        $order->current_price_per_item = $product->price;
        $order->user()->associate($this->user);
        $order->product()->associate($product);
        $order->status = OrderStatus::COMPLETED;

        if(!$order->save()){
            throw new TestException('Order Gagal!');
        }

        return $order;
    }

    /**
     * @param Order $order
     * @param $data
     * @return void
     * @throws TestException
     */
    protected function createPayment(Order $order, $data){
        $payment = new Payment();
        $payment->fill($data);
        $payment->total_pay = $order->total_amount;
        $payment->order()->associate($order);

        if(!$payment->save()){
            throw new TestException('Payment Gagal!');
        }

    }

    /**
     * @param Order $order
     * @throws TestException
     */
    protected function checkReward(Order $order){
        if($order->total_amount >= 20000 && $order->total_amount < 40000){
            $reward = new Reward();
            $reward->user()->associate($this->user);
            $reward->order()->associate($order);
            $reward->point = 20;
            if(!$reward->save()){
                throw new TestException('Failed Save Reward!');
            }
        }

        if($order->total_amount >= 40000){
            $reward = new Reward();
            $reward->user()->associate($this->user);
            $reward->order()->associate($order);
            $reward->point = 40;
            if(!$reward->save()){
                throw new TestException('Failed Save Reward!');
            }
        }
    }

    /**
     * @return mixed
     */
    public function listCustomer()
    {
        return User::where('role', Role::CUSTOMER)
                    ->whereHas('orders',function(Builder $query){
                        $query->whereHas('product', function (Builder $query){
                            $query->where('user_id', $this->user->id);
                        });
                    })
                    ->get();
    }
}
