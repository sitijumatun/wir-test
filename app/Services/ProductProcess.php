<?php


namespace App\Services;


use App\Exceptions\TestException;
use App\Product;
use App\User;
use Illuminate\Database\Eloquent\Collection;

class ProductProcess extends AbstractServiceConstant
{
    /**
     * @return Product[]|Collection
     */
    public function index(){
        return Product::all();
    }

    /**
     * @param $data
     * @return Product
     * @throws TestException
     */
    public function store($data){
        $product = new Product();
        $product->fill($data);
        $product->user()->associate($this->user);
        if(!$product->save()){
            throw new TestException('Data Gagal Disimpan!');
        }

        return $product;
    }

    /**
     * @param Product $product
     * @param $data
     * @return Product
     * @throws TestException
     */
    public function update(Product $product, $data){
        $product->fill($data);
        if(!$product->update()){
            throw new TestException('Data Gagal DiUpdate!');
        }

        return $product;

    }

    /**
     * @param Product $product
     * @return bool
     * @throws TestException
     */
    public function delete(Product $product){
        if(!$product->delete()){
            throw new TestException('Data Gagal DiDelete!');
        }

        return true;
    }

}
