<?php


namespace App\Services;


use App\Exceptions\TestException;
use App\User;

class UserProcess
{
    /**
     * @param $data
     * @return User
     * @throws TestException
     */
    public function storeData($data){
        $user = new User();
        $user->fill($data);
        $user->password = bcrypt($data['password']);
        if(!$user->save()){
            throw new TestException('Data Gagal Disimpan!');
        }

        return $user;
    }

}
