<?php

namespace App\Transformers;

use App\Constants\OrderStatus;
use App\Order;
use League\Fractal\TransformerAbstract;
use ReflectionException;

class OrderTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param Order $order
     * @return array
     * @throws ReflectionException
     */
    public function transform(Order $order)
    {
        $result = [
            'id' => $order->id,
            'sumOfItem' => $order->sum_of_item,
            'currentPricePerItem' => $order->current_price_per_item,
            'customerDetail' => [
                'name' => $order->user->name,
                'email' => $order->user->email,
                'phoneNumber' => $order->user->phone_number
            ],
            'statusOrder' => OrderStatus::getTitle($order->status),
            'product' => fractal($order->product, new ProductTransformer())->toArray()
        ];

        if($order->reward){
            $result +=[
              'rewardPoint' => $order->reward->point
            ];
        }

        return $result;
    }
}
