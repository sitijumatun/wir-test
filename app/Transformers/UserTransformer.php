<?php

namespace App\Transformers;

use App\Constants\Role;
use App\User;
use League\Fractal\TransformerAbstract;
use ReflectionException;

class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param User $user
     * @return array
     * @throws ReflectionException
     */
    public function transform(User $user)
    {
        $result = [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'token' => $user->createToken('MyApp')->accessToken,
            'role' => Role::getTitle($user->role),
            'phoneNumber' => $user->phone_number
        ];

        if($user->role === Role::CUSTOMER){
            $result +=[
                'point' => $user->rewards ?  $user->rewards()->get()->sum('point') : 0,
            ];
        }

        return $result;
    }
}
