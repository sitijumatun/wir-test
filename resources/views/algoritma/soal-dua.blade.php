@extends('layouts.layout')

@section('content')
    <div class="tab-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Jawaban 2</div>
                        <div class="card-body">
                            <form method="get" action="">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="price">Input Number To Make Pattern</label>
                                        <input type="number" min="0" class="form-control" name="number" id="number" placeholder="number of pattern">
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if($countInput > 0 && $inputData)
            <div class="container" style="margin-top: 30px">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Hasil 1b With Input Data <b>{{ number_format($inputData, 0, ',', '.')}}</b></div>
                            <div class="card-body">
                                @php
                                    for ($i=0; $i<=$countInput; $i++){
                                        $data = substr($inputData, $i);
                                        $dataFirst = substr($data, 0,1);
                                        $countData = strlen($data);
                                        for($j=1; $j<$countData; $j++){
                                            $dataFirst .= 0;
                                        }
                                        echo $dataFirst;
                                        echo "<br>";
                                    }
                                @endphp
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
