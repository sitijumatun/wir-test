@extends('layouts.layout')

@section('content')
    <div class="tab-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Jawaban 1b</div>
                        <div class="card-body">
                            <form method="get" action="">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="price">Input Number To Make Pattern</label>
                                        <input type="number" min="0" class="form-control" name="number" id="number" placeholder="number of pattern">
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if($numberLoop > 0)
            <div class="container" style="margin-top: 30px">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Hasil 1b With Input <b>{{ $numberLoop }}</b></div>
                            <div class="card-body">
                                @php
                                    $input = $numberLoop;
                                    $loopNumber = $input-1;

                                    $result = "";
                                    for($row = 0; $row < $loopNumber; $row++) {
                                        for($col = 0; $col < $loopNumber; $col++) {
                                            if (($row > 0 && $col == $loopNumber-1) || ($row == 0 && $col == $loopNumber-1) || ($row == $loopNumber-1 && $col == $loopNumber-1)) {
                                                $result .= "#\r\n";
                                            } else if ($row == 0 || ($row > 0 && $col == 0) || $row == $col || $row == $loopNumber-1 || ($col == $loopNumber-$row-1)) {
                                                $result .= "#";
                                            } else {
                                                $result .= " ";
                                            }
                                        }
                                    }

                                    dd($result);
                                @endphp
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
