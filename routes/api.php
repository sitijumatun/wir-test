<?php

use App\Constants\Role;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'auth', 'namespace'=> 'API'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
});

Route::group(['middleware' => 'auth:api', 'namespace' => 'API'], static function () {
    Route::group(['middleware' => 'role:' . Role::MERCHANT], static function () {
        Route::post('create-product', 'ProductController@store');
        Route::post('update-product/{product}', 'ProductController@update');
        Route::get('delete-product/{product}', 'ProductController@delete');
        Route::get('list-customer', 'OrderController@listCustomer');
    });

    Route::group(['middleware' => 'role:' . Role::CUSTOMER.','.Role::MERCHANT], static function () {
        Route::get('list-product', 'ProductController@list');
    });

    Route::group(['middleware' => 'role:' . Role::CUSTOMER], static function () {
        Route::post('buy-product', 'OrderController@buy');
    });
});



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

