<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home-page');
});

Route::group(['namespace' => 'Web'], static function() {
    Route::get('program-1a', 'AlgoritmaController@programSatuA')->name('program.satu.a');
    Route::get('program-1b', 'AlgoritmaController@programSatuB')->name('program.satu.b');
    Route::get('program-2', 'AlgoritmaController@programDua')->name('program.dua');
});


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
